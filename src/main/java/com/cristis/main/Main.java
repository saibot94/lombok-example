package com.cristis.main;

import com.cristis.dto.Book;
import com.cristis.dto.Magazine;

/**
 * Created by darkg on 2/22/2017.
 */
public class Main {
    public static void main(String[] args) {
        Book book1 = new Book(1, "Padurea spanzuratilor");
        Book book2 = new Book(1, "Padurea spanzuratilor");

        System.out.println(book1.equals(book2));


        // No setters created
        Magazine mag1 = new Magazine(1, "Chic");
        System.out.println(mag1.getId() + " " + mag1.getName());
        System.out.println(mag1);
    }
}
