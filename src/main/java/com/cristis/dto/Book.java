package com.cristis.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by darkg on 2/23/2017.
 */

@EqualsAndHashCode
@Data
@AllArgsConstructor
public class Book {
    private int bookId;
    private String bookName;
}
