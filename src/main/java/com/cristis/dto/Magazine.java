package com.cristis.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * Created by darkg on 2/23/2017.
 */

@AllArgsConstructor
@ToString
public class Magazine {
    @Getter
    private int id;
    @Getter
    private String name;


}
